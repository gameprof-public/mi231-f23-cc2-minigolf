################################################
<<<<<<<< HEAD:Assets/___Student Folders/Mo's READ ME.txt
# Project   - Coding Challenge 2 | Minigolf
# Your Name - Mohamed Ahmed
========
# Project   - CC2 Golf
# Your Name - Ravi G Kumar
>>>>>>>> origin/RaviGKumar:Assets/___Student Folders/RaviGKumar/ReadMe_Template.txt
# Date      - 10/18/2023
################################################

___What are the controls to your game? How do we play?___

mouse


___What creative additions did you make? How can we find them?___


portals

___Any assets used that you didn't create yourself? (art, music, etc. Just tell us where you got it, link it here)___

nope


___Did you receive help from anyone outside this class? (list their names and what they helped with)___

prof. jeremy helped me with the portal code

___Did you get help from any AI Code Assistants? (Tell us which .cs file to look in for the citation and describe what you learned)___

no

___Did you get help from any online websites, videos, or tutorials? (link them here)___


no

___What trouble did you have with this project?___

getting the portals to work

___Is there anything else we should know?___

nope


