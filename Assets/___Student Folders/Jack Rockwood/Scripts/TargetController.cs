using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class TargetController : MonoBehaviour
{
    private bool isAttacked = false;

    static public int numTargets = 5;

    void OnCollisionEnter(Collision other)
    {
        ;
        if (other.gameObject.tag == "Ball")
        {
            isAttacked = true;
            
        }
    }

    // Start is called before the first frame update
    void Start()
    {       
        
        
        
    }

    // Update is called once per frame
    void Update()
    {
        

        
        if (isAttacked)
        {           
            Destroy(this.gameObject);
            numTargets -= 1;
        }
        
    }
}
