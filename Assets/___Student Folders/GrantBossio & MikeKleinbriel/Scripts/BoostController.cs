using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoostController : MonoBehaviour
{
    public BoxCollider padCollider;
    // We've found that 500 produces the most reasonable results
    public float boostSpeed = 500f;
    
    // Start is called before the first frame update
    void Start()
    {
        padCollider = GetComponent<BoxCollider>();
    }

    private void OnTriggerStay(Collider other)
    {
        // Get the rigidbody from the ball
        Rigidbody rb = other.GetComponent<Rigidbody>();
        Vector3 boost = transform.forward;
        rb.AddForce(boost * boostSpeed * 10, ForceMode.Force);
        Debug.Log("Boost Pad Hit!");
    }
}
