using System;
using UnityEngine;

public class GravityField : MonoBehaviour
{
    
    [Header(("Inscribed"))]
    public float fieldStrength = 150f;
    
    
    [Header("Sphere Settings")] 
    
    public bool isSphere = false;

    public bool toCenter = true;

    public Material pullMaterial;
    public Material pushMaterial;
    
    [Header("Dynamic")]
    public BoxCollider boxField;
    public SphereCollider sphereField;
    public Rigidbody rb;
    public Transform target;
    public MeshRenderer mat;

    private void Start()
    {
        if (isSphere)
        {
            sphereField = GetComponent<SphereCollider>();
        }
        else
        {
            boxField = GetComponent<BoxCollider>();
        }

        mat = GetComponent<MeshRenderer>();
    }

    private void Update()
    {
        if (isSphere)
        {
            if (toCenter)
            {
                mat.material = pullMaterial;
            }
            else
            {
                mat.material = pushMaterial;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        target = other.GetComponent<Transform>();
        rb = other.GetComponent<Rigidbody>();
        rb.useGravity = false;
    }

    private void OnTriggerStay(Collider other)
    {
        Vector3 direction;
        if (isSphere)
        {
            direction = target.position - transform.position;
        }
        else
        {
            direction = -transform.up;
        }

        Vector3 gravityDirection;
        if (isSphere)
        {
            if (toCenter)
            {
                gravityDirection = -direction;
            }
            else
            {
                gravityDirection = direction;
            }
        }
        else
        {
            gravityDirection = direction;
        }
        
        rb.AddForce(gravityDirection * fieldStrength * 10, ForceMode.Force);
        Debug.Log("Gravity Field Entered");
    }

    private void OnTriggerExit(Collider other)
    {
        target = null;
        rb.useGravity = true;
        rb = null;
    }
}
