################################################
# Project   - Minigolf
# Your Name - Mike Kleinbriel and Grant Bossio
# Date      - 10/18/23
################################################

___What are the controls to your game? How do we play?___
Standard controls



___What creative additions did you make? How can we find them?___
Boost pad - Adds force to player in boost pad direction

Gravity field - Makes ball be affected by a local gravity field when inside it. There is a box and sphere version. The sphere
can push or pull the ball. Pull wasn't used due to the golf ball getting stuck a lot



___Any assets used that you didn't create yourself? (art, music, etc. Just tell us where you got it, link it here)___

The assets were provided and all made in unity

___Did you receive help from anyone outside this class? (list their names and what they helped with)___

We did not receive any help from anyone outside this class

___Did you get help from any AI Code Assistants? (Tell us which .cs file to look in for the citation and describe what you learned)___

We did not get any help from any AI Code Assistants

___Did you get help from any online websites, videos, or tutorials? (link them here)___

We did not get help from any online websites, videos, or tutorials


___What trouble did you have with this project?___

Making the level was the hardest part... My level prefab got wiped when trying to merge so I had to remake mine - Mike

___Is there anything else we should know?___

We have nothing else to add

