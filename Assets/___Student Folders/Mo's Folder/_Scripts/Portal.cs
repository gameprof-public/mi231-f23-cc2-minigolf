using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour
{
	static Portal CURRENT = null;

	public GameObject		OtherPortal;
	public Vector3			otherPortalPos;

	// Start is called before the first frame update
	void Start()
	{
		Debug.Log("Portal Initialized");
		otherPortalPos = OtherPortal.transform.position;	
	}

	private void OnTriggerEnter(Collider ball)
	{
		if (CURRENT == this) return;
		if (ball.CompareTag("Ball"))
		{
			CURRENT = OtherPortal.GetComponent<Portal>();
			ball.transform.position = new Vector3(otherPortalPos.x, otherPortalPos.y, otherPortalPos.z);
		}
	}

	public void OnTriggerExit(Collider other)
	{
		if (CURRENT == this) CURRENT = null;
	}

}
