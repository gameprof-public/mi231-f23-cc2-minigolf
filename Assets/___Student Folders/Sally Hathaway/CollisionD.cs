using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollisionD : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        
        if (collision.gameObject.CompareTag("Ball"))
        {
            Debug.Log("Ball Hit");
            SceneManager.LoadScene("_Scene_Main_SallyHathaway");
        }
    }
}
