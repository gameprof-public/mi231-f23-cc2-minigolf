################################################
# Golf Course Coding Challenge 2 
# Kenzie Hopson & Riley Baker
# 10/19/23 
################################################

___What are the controls to your game? How do we play?___
Using left click on your mouse, drag and release to put the ball. Try to get the ball into the hole.



___What creative additions did you make? How can we find them?___
Rotating spinner obstacle



___Any assets used that you didn't create yourself? (art, music, etc. Just tell us where you got it, link it here)___
No



___Did you receive help from anyone outside this class? (list their names and what they helped with)___
No


___Did you get help from any AI Code Assistants? (Tell us which .cs file to look in for the citation and describe what you learned)___
No


___Did you get help from any online websites, videos, or tutorials? (link them here)___
https://www.youtube.com/watch?v=lyl_a8y_DCg



___What trouble did you have with this project?___
Scripts


___Is there anything else we should know?___
No


