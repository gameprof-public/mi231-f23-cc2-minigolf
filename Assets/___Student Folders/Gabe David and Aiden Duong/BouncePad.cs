using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BouncePad : MonoBehaviour
{
   [Tooltip("Acceleration values are better in the thousands and higher")]
    public float XAcceleration = 0;
    [Tooltip("Acceleration values are better in the thousands and higher")]
    public float YAcceleration = 1500;
    [Tooltip("Acceleration values are better in the thousands and higher")]
    public float ZAcceleration = 0;

    private void OnCollisionEnter(Collision coll)
    {
        Rigidbody ballrigid = coll.gameObject.GetComponent<Rigidbody>();
        ballrigid.AddForce(XAcceleration, YAcceleration, ZAcceleration,ForceMode.Acceleration);
    }
    
    
}
