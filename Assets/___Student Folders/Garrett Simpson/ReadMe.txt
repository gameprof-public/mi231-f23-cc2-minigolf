################################################
# Project   - Coding Challenge 2
# Your Name - Garrett Simpson
# Date      - 10/18/23
################################################

___What are the controls to your game? How do we play?___

use your mouse to click and drag the golf ball. Letting go makes it move the opposite direction
of the direction you're dragging your mouse. 


___What creative additions did you make? How can we find them?___

-added a spinning wheel object with adjustable rotation speed


___Any assets used that you didn't create yourself? (art, music, etc. Just tell us where you got it, link it here)___




___Did you receive help from anyone outside this class? (list their names and what they helped with)___



___Did you get help from any AI Code Assistants? (Tell us which .cs file to look in for the citation and describe what you learned)___



___Did you get help from any online websites, videos, or tutorials? (link them here)___




___What trouble did you have with this project?___



___Is there anything else we should know?___



