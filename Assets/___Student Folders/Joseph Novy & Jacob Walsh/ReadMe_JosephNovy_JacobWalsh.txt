################################################
# Project   - CC2
# Your Name - Joseph Novy Jacob Walsh
# Date      - 10/18/2023
################################################

___What are the controls to your game? How do we play?___
Mouse to shoot golf ball. R to reset ball



___What creative additions did you make? How can we find them?___
Added a cannon that fires ball when ball is shot into it



___Any assets used that you didn't create yourself? (art, music, etc. Just tell us where you got it, link it here)___
none



___Did you receive help from anyone outside this class? (list their names and what they helped with)___
none


___Did you get help from any AI Code Assistants? (Tell us which .cs file to look in for the citation and describe what you learned)___
none


___Did you get help from any online websites, videos, or tutorials? (link them here)___
none



___What trouble did you have with this project?___
none


___Is there anything else we should know?___
none


