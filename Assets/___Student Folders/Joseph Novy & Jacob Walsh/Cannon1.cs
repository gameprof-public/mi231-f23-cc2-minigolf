using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;

public class Cannon1 : MonoBehaviour
{
    [Header("Cannon")]
    public float launchSpeed = 10f;
    [Range(0, 90)]
    public float launchAngle = 30f;

    public GameObject anchor;

    void OnCollisionEnter(Collision collision)
    {
        // Get ball object
        GameObject ball = collision.gameObject;
        Rigidbody rigid = ball.GetComponent<Rigidbody>();

        // Snap to anchor pos
        ball.transform.position = anchor.transform.position;

        // Set ball velocity based on angle
        Vector3 vel = Vector3.zero;

        // Convert angle to radians
        float rads = launchAngle * Mathf.Deg2Rad;

        // Trig calculations
        float z = launchSpeed * Mathf.Cos(rads);
        float y = launchSpeed * Mathf.Sin(rads);
        vel.z = z;
        vel.y = y;



        // Apply velocity
        rigid.velocity = vel;
    }
}
