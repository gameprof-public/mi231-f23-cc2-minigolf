using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spinner : MonoBehaviour
{
    public bool spinReverse = false;
    public float spinValue = -0.5f;
    // Start is called before the first frame update
    void Start()
    {
        if(spinReverse == true)
        {
            spinValue = spinValue * -1;
        }
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0f, 0f, spinValue, Space.Self);
    }
}
