################################################
# Project   - MiniGolf Coding Challenge
# Your Name - Daniel Zrull & JJ Sanchez
# Date      - 10/18/2023
################################################

___What are the controls to your game? How do we play?___

Pull the golf ball back with the mouse and try to get the ball in the hole.


___What creative additions did you make? How can we find them?___

N/A


___Any assets used that you didn't create yourself? (art, music, etc. Just tell us where you got it, link it here)___

N/A


___Did you receive help from anyone outside this class? (list their names and what they helped with)___

N/A

___Did you get help from any AI Code Assistants? (Tell us which .cs file to look in for the citation and describe what you learned)___

N/A

___Did you get help from any online websites, videos, or tutorials? (link them here)___

JJ's moving platforms and their code is based on the same platforms used in JJ's Mission Demolition submission, which are themselves based on similar platforms created in Brian Winn's game design class.


___What trouble did you have with this project?___

None

___Is there anything else we should know?___

No

