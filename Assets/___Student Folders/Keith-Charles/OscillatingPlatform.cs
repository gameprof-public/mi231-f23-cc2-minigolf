using JetBrains.Rider.Unity.Editor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class OscillatingPlatform : MonoBehaviour
{
    private Rigidbody rb;

    public float duration = 5f;
    private float startTime;

    public Vector3 direction = Vector3.up;
    public float distance;

    public float cooldown = 1.5f;

    private Vector3 startingPos;
    private Vector3 EndingPos => startingPos + direction * distance;

    private bool towardsDirection;
    

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        startingPos = transform.position;
        startTime = Time.time + cooldown;
        towardsDirection = true;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Time.time > startTime + duration)
        {
            towardsDirection = !towardsDirection;
            startTime = Time.time + cooldown;
        }

        float progress01 = (Time.time - startTime) / duration;

        Vector3 newPos;
        if (towardsDirection)
        {
            newPos = Vector3.Lerp(startingPos, EndingPos, progress01);
        }
        else
        {
            newPos = Vector3.Lerp(EndingPos, startingPos, progress01);
        }
        rb.MovePosition(newPos);
    }

    private void OnTriggerStay(Collider other)
    {
        if (TryGetComponent(out Golfball ball))
        {
            ball.GetComponent<Rigidbody>().WakeUp();
        }
    }
}
