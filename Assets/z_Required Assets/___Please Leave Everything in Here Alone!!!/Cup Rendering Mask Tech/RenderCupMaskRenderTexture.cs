using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

// This is based on fantastic work from Josh Sauter's fantastic game Superspective

[ExecuteInEditMode]
[RequireComponent( typeof(Camera) )]
public class RenderCupMaskRenderTexture : MonoBehaviour
{
    
    public static readonly int CupMask = Shader.PropertyToID("_CupMask");
    
    [HideInInspector]
    public Camera cam;
    
    
    [ReadOnly]
    public RenderTexture cupMaskRT;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if ( !IsCameraRefValid() ) return;

        IsRenderTextureValid();
    }
    
    
    bool IsCameraRefValid() {
        if (cam == null) cam = this.GetComponent<Camera>();
        if ( cam == null ) {
            Debug.LogError( $"No Camera was found on the GameObject {gameObject.name}!" );
            return false;
        }
        return true;
    }

    bool IsRenderTextureValid() {
        if ( cupMaskRT == null ) {
            CreateRenderTexture( Screen.width, Screen.height, out cupMaskRT, cam );
            if ( cupMaskRT == null ) {
                Debug.LogError("Failed to CreateRenderTexture");
                return false;
            }
        }
        return true;
    } 
    
    RenderTexture CreateRenderTexture(int currentWidth, int currentHeight, out RenderTexture rt, Camera targetCamera) {
        rt = new RenderTexture(currentWidth, currentHeight, 24, RenderTextureFormat.ARGB32);
        rt.enableRandomWrite = true;
        rt.Create();

        // Josh uses these, but I don't. - JGB 2023-10-14
        // These are set up like: static readonly int ResolutionX = Shader.PropertyToID("_ResolutionX");
        // Shader.SetGlobalFloat(ResolutionX, currentWidth);
        // Shader.SetGlobalFloat(ResolutionY, currentHeight);

        if (targetCamera != null) {
            targetCamera.targetTexture = rt;
        }

        return rt;
    }

    private void OnPostRender() {
        if ( !IsCameraRefValid() ) return;
        if ( !IsRenderTextureValid() ) return;
        // Set a global texture that can be accessed by any ShaderLab shader
        Shader.SetGlobalTexture(CupMask, cupMaskRT);
    }
}
