using System;
using UnityEngine;
using System.Collections.Generic;
using UnityEditor;
using NaughtyAttributes;

namespace XnTools {
    [System.Serializable]
    public class XnLog {
        private const int maxBoxLines = 16;
        private const int maxLogInfos = 64;
        private const string hidingMessage = "(Hiding text while playing to improve performance.)" +
                                             "\n(If you pause or uncheck the option below, you'll see more)";
        
        [ResizableTextArea()][SerializeField]
        protected string logText;
        [SerializeField] protected bool clearOnStart = true;
        [SerializeField] protected bool hideLogTextWhilePlaying = true;
        
        private List<LogInfo> logInfos = new List<LogInfo>();
        private bool          inited   = false;

        public void Init() {
            if ( clearOnStart ) Clear();
            logText = hidingMessage;
            
            // This forces UpdateHideLog() to be called
            prevHideLogTextWhilePlaying = !hideLogTextWhilePlaying;
            
#if UNITY_EDITOR
            UnityEditor.EditorApplication.pauseStateChanged += PauseLog;
            UnityEditor.EditorApplication.update += CheckHideLogCheckbox;
#endif
            inited = true;
        }

        public void CleanUp() {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.pauseStateChanged -= PauseLog;
            UnityEditor.EditorApplication.update -= CheckHideLogCheckbox;
#endif
            UpdateLogText( true );
        }

        void CheckHideLogCheckbox() {
            if ( hideLogTextWhilePlaying != prevHideLogTextWhilePlaying ) {
                UpdateHideLog();
            }
        }

        void PauseLog(PauseState ps) {
            if (ps == PauseState.Paused) {
                Log($"====== Paused  at: {Time.time:0.00} or realtime: {Time.realtimeSinceStartup:0.00}.");
                UpdateLogText(true);
            } else {
                // This forces Log to think about hiding or not
                prevHideLogTextWhilePlaying = !hideLogTextWhilePlaying;
                // This message will also clear the text while playing if hideLogTextWhilePlaying == true
                Log($"====== Resumed at: {Time.time:0.00} or realtime: {Time.realtimeSinceStartup:0.00}.");
            }
        }


        public void Log( params string[] bits ) {
            Log( null, bits );
        }
        public void Log( object obj, params string[] bits ) {
            if ( !inited ) {
                Init();
            }
            string oN = "";
            if (obj != null) {
                oN = obj.GetType().ToStringShort();
            }
            
            logInfos.Add( new LogInfo() {
                objName = oN,     
                txt = String.Join( "\t", bits ),
                time = $"{(Time.time/60):##0.00}",
                toString = ""
            } );
            
            if ( hideLogTextWhilePlaying != prevHideLogTextWhilePlaying ) {
                UpdateHideLog();
            } else if ( !hideLogTextWhilePlaying ) {
                UpdateLogText();
            }
        }

        public void UpdateHideLog() {
            if ( Application.isPlaying ) {
                if ( hideLogTextWhilePlaying ) {
                    logText = hidingMessage;
                } else {
                    UpdateLogText();
                }
            }
            prevHideLogTextWhilePlaying = hideLogTextWhilePlaying;
        }

        private bool prevHideLogTextWhilePlaying = false;
        public void UpdateLogText(bool allMessages = false) {
            while ( logInfos.Count > maxLogInfos ) {
                logInfos.RemoveAt(0);
            }

            if (allMessages) {
                logText = String.Join("\n", logInfos);
            } else {
#if NETSTANDARD
                logText = String.Join("\n", logInfos.GetRange(^maxBoxLines..));
#else
                logText = String.Join( "\n", logInfos.GetRange( (-maxBoxLines, 1000) ) );
#endif
            }
        }

        public void Clear( bool leaveMostRecentLines = false ) {
            // if ( leaveMostRecentLines ) {
            //     logInfos.RemoveRange(maxBoxLines, 99999);
            // }
            logInfos.Clear();
        }

        public struct LogInfo {
            public string time;
            public string objName;
            public string txt;
            public string toString;

            public override string ToString() {
                if ( toString == "" ) {
                    if ( objName == "" ) {
                        toString = $"• {time} - {txt}";
                    } else {
                        toString = $"• {time} - [ {objName} ] - {txt}";
                    }
                }
                return toString;
            } 
        }
    }
}
