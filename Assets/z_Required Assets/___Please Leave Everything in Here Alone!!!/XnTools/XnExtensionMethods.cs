using UnityEngine;
using System.Collections.Generic;
using System.Reflection;
using Random = UnityEngine.Random;
using System;


namespace XnTools {
	public static class XnUtilsExtensionMethods {

		#region int Extensions

		/// <summary>
		/// A version of Mathf.Pow() that should be faster because it doesn't accept non-integer powers.
		/// This int version also does not accept negative powers. 
		/// </summary>
		/// <param name="n"></param>
		/// <param name="pow"></param>
		/// <returns></returns>
		static public float Pow(this int n, int pow) {
			float res = 1f;

			if (pow < 0) {
				return 0;
			}

			for (int i = 1; i <= pow; i++) {
				res *= n;
			}
			return res;
		}

		#endregion

		#region float Extensions

		/// <summary>
		/// A version of Mathf.Pow() that should be faster because it doesn't accept non-integer powers
		/// </summary>
		/// <param name="n"></param>
		/// <param name="pow"></param>
		/// <returns></returns>
		static public float IntPow(this float n, int pow) {
			float res = 1f;

			if (pow < 0) {
				pow *= -1;
				n = 1f / n;
			}

			for (int i = 1; i <= pow; i++) {
				res *= n;
			}
			return res;
		}

		// Note: The decompiled code of Mathf.Approximately is:
		// public static bool Approximately(float a, float b) => (double) Mathf.Abs(b - a)
		//   < (double) Mathf.Max(1E-06f * Mathf.Max(Mathf.Abs(a), Mathf.Abs(b)), Mathf.Epsilon * 8f);
		// So it already takes into account the same issues discussed in the post at:
		//    https://roundwide.com/equality-comparison-of-floating-point-numbers-in-csharp/

		/// <summary>
		/// Compares two floats to see if they are approximately the same
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <param name="resolution">The allowed difference between them
		/// (e.g., res=1 returns true if Mathf.Abs(a-b) is less than or equal to 1 )</param>
		/// <returns></returns>
		static public bool IsApproximately(this float a, float b, float resolution = 0) {
			if (resolution == 0) {
				return Mathf.Approximately(a, b);
			}
			return a.PreciseEquality(b, resolution);
			// return ( Mathf.Abs( a - b ) <= resolution );
		}

		/// <summary>
		/// Compare equality while considering the limits of floating point error.
		/// From: https://roundwide.com/equality-comparison-of-floating-point-numbers-in-csharp/
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <param name="tolerance">Default value is 1e-6</param>
		/// <returns></returns>
		static public bool PreciseEquality(this float a, float b, float tolerance = (1e-6f)) {
			float diff = Mathf.Abs(a - b);

			return diff <= tolerance ||
			       diff <= Mathf.Max(Mathf.Abs(a), Mathf.Abs(b)) * tolerance;
		}

		static public float LerpTo(this float a, float b, float u) {
			return (1 - u) * a + u * b;
		}

		/// <summary>
		/// Uses System.BitConverter to tell whether a float is a whole number faster than other methods.
		/// I got this from Chat-GPT.
		/// </summary>
		/// <param name="num">The float to be tested</param>
		/// <returns>true if num is a whole number</returns>
		static public bool IsAnInteger(this float num) {
			// From Chat-GPT
			// The BitConverter.GetBytes() method takes a float value num as input and returns its binary representation
			// as a byte array. Then, the BitConverter.ToInt32() method is used to convert that byte array to an int
			// representation, which allows us to inspect the individual bits of the floating-point number.
			int intRepresentation = System.BitConverter.ToInt32(System.BitConverter.GetBytes(num), 0);
			// The intRepresentation value is a 32-bit integer, where the least significant 23 bits represent the
			// significand (mantissa) of the floating-point number. The 0x007FFFFF is a bitmask that has all bits
			// set to 1 in the least significant 23 positions, which is used to extract the significand bits using
			// a bitwise AND operation (&).
			return (intRepresentation & 0x007FFFFF) == 0;
		}

		#endregion

		#region Binary Number to String Extension Methods

		static public string ToStringBinary(this ulong u, char zeroChar = '_') {
			string s = System.Convert.ToString((long)u, 2);
			if (zeroChar != '0') s = s.Replace('0', zeroChar);
			s = s.PadLeft(64, zeroChar);
			return s;
		}

		static public string ToStringBinary(this uint u, char zeroChar = '_') {
			string s = System.Convert.ToString(u, 2);
			if (zeroChar != '0') s = s.Replace('0', zeroChar);
			s = s.PadLeft(32, zeroChar);
			return s;
		}

		static public string ToStringBinary(this int u, char zeroChar = '_') {
			string s = System.Convert.ToString(u, 2);
			if (zeroChar != '0') s = s.Replace('0', zeroChar);
			s = s.PadLeft(32, zeroChar);
			return s;
		}

		#endregion

		#region Hamming Weight Extension Methods

		static public int HammingWeight(this ulong u) {
			u = u - ((u >> 1) & 0x5555555555555555UL);
			u = (u & 0x3333333333333333UL) + ((u >> 2) & 0x3333333333333333UL);
			return (int)(unchecked(((u + (u >> 4)) & 0xF0F0F0F0F0F0F0FUL) * 0x101010101010101UL) >> 56);
		}

		static public int HammingWeight(this uint u) {
			u = u - ((u >> 1) & 0x55555555U);
			u = (u & 0x33333333U) + ((u >> 2) & 0x33333333U);
			return (int)(unchecked(((u + (u >> 4)) & 0xF0F0F0FU) * 0x1010101U) >> 24);
		}

		static public int HammingWeight(this int u) {
			return ((uint)u).HammingWeight();
		}

		static public int HammingWeight(this LayerMask lm) {
			return ((uint)(int)lm).HammingWeight();
		}

		static public void TestHammingWeight() {
			int numTestsPerType = 100;
			int numBits, numBitsLeft;
			int possibleBits;
			int hammingWeight;
			int correctChecks;
			System.Text.StringBuilder sb = new System.Text.StringBuilder();

			// ulong check
			sb.AppendLine("__________ ulong tests __________");
			ulong ul;
			possibleBits = 64;
			correctChecks = 0;

			for (int i = 0; i < numTestsPerType; i++) {
				numBits = Random.Range(0, possibleBits + 1);
				numBitsLeft = numBits;
				ul = 0UL;

				for (int j = possibleBits - 1; j >= 0; j--) {
					if (numBitsLeft == 0) continue;

					if ((numBitsLeft == j + 1) || XnUtils.randomBool) {
						ul |= (1UL << j);
						numBitsLeft--;
					}
				}
				hammingWeight = ul.HammingWeight();

				if (hammingWeight == numBits) {
					sb.AppendLine($"_pass_ - {ul.ToStringBinary()} has {hammingWeight} bits");
					correctChecks++;
				} else {
					sb.AppendLine(
						$"FAILED - {ul.ToStringBinary()} should have {numBits} bits, but HammingWeight counted {hammingWeight} bits");
				}
			}

			Debug.LogWarning(
				$"{correctChecks} / {numTestsPerType} tests passed ({(float)correctChecks / numTestsPerType * 100}%)\n" +
				sb.ToString());

			// uint checks
			sb.Clear();
			sb.AppendLine("__________ uint tests __________");
			uint ui;
			possibleBits = 32;
			correctChecks = 0;

			for (int i = 0; i < numTestsPerType; i++) {
				numBits = Random.Range(0, possibleBits + 1);
				numBitsLeft = numBits;
				ui = 0U;

				for (int j = possibleBits - 1; j >= 0; j--) {
					if (numBitsLeft == 0) continue;

					if ((numBitsLeft == j + 1) || XnUtils.randomBool) {
						ui |= (1U << j);
						numBitsLeft--;
					}
				}
				hammingWeight = ui.HammingWeight();

				if (hammingWeight == numBits) {
					sb.AppendLine($"_pass_ - {ui.ToStringBinary()} has {hammingWeight} bits");
					correctChecks++;
				} else {
					sb.AppendLine(
						$"FAILED - {ui.ToStringBinary()} should have {numBits} bits, but HammingWeight counted {hammingWeight} bits");
				}
			}

			Debug.LogWarning(
				$"{correctChecks} / {numTestsPerType} tests passed ({(float)correctChecks / numTestsPerType * 100}%)\n" +
				sb.ToString());

			// uint checks
			sb.Clear();
			sb.AppendLine("__________ int tests __________");
			int n;
			possibleBits = 32;
			correctChecks = 0;

			for (int i = 0; i < numTestsPerType; i++) {
				numBits = Random.Range(0, possibleBits + 1);
				numBitsLeft = numBits;
				n = 0;

				for (int j = possibleBits - 1; j >= 0; j--) {
					if (numBitsLeft == 0) continue;

					if ((numBitsLeft == j + 1) || XnUtils.randomBool) {
						n |= (1 << j);
						numBitsLeft--;
					}
				}
				hammingWeight = n.HammingWeight();

				if (hammingWeight == numBits) {
					sb.AppendLine($"_pass_ - {n.ToStringBinary()} has {hammingWeight} bits");
					correctChecks++;
				} else {
					sb.AppendLine(
						$"FAILED - {n.ToStringBinary()} should have {numBits} bits, but HammingWeight counted {hammingWeight} bits");
				}
			}

			Debug.LogWarning(
				$"{correctChecks} / {numTestsPerType} tests passed ({(float)correctChecks / numTestsPerType * 100}%)\n" +
				sb.ToString());
		}

		#endregion


		#region List Extensions

		/// <summary>
		/// Returns a valid random index (i.e., element number) for this list
		/// </summary>
		/// <param name="list"></param>
		/// <typeparam name="T"></typeparam>
		/// <returns>A valid random index (i.e., element number) for this list</returns>
		static public int RandomIndex<T>(this List<T> list) {
			if (list.Count == 0) return -1;
			return Random.Range(0, list.Count);
		}

		/// <summary>
		/// Returns a random element T from this list
		/// </summary>
		/// <param name="list"></param>
		/// <typeparam name="T"></typeparam>
		/// <returns>A random element T from this list</returns>
		static public T RandomElement<T>(this List<T> list) where T : class {
			if (list.Count == 0) return null;
			return list[list.RandomIndex()];
		}

		/// <summary>
		/// Removes and returns a random element T from this list
		/// </summary>
		/// <param name="list"></param>
		/// <typeparam name="T"></typeparam>
		/// <returns>The random element T that was deleted from this list</returns>
		static public T RemoveRandom<T>(this List<T> list) where T : class {
			if (list.Count == 0) return null;

			int n = list.RandomIndex();
			T item = list[n];
			list.RemoveAt(n);
			return item;
		}
		// [CanBeNull]
		// static public T? RemoveRandom<T>(this List<T> list) where T : struct {
		// 	if (list.Count == 0) return null;
		// 	
		// 	int n = list.RandomIndex();
		// 	T item = list[n];
		// 	list.RemoveAt(n);
		// 	return item;
		// }

		/// <summary>
		/// Extends List.AddRange to allow individual items to be passed in via params.
		/// </summary>
		/// <param name="list"></param>
		/// <param name="items"></param>
		/// <typeparam name="T"></typeparam>
		static public void AddRange<T>(this List<T> list, params T[] items) {
			if (items.Length > 0) list.AddRange(items);
		}

		/// <summary>
		/// A version of ToString() that expands the values within the List 
		/// </summary>
		/// <returns>A string of the List that shows all elements</returns>
		static public string ToStringExpanded<T>(this List<T> list, bool addLineBreaks = false) {
			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			// sb.Append( addLineBreaks ?  "\n[ " : "[ " );
			sb.Append("[ ");

			for (int i = 0; i < list.Count; i++) {
				if (addLineBreaks) {
					if (i == 0) {
						sb.Append($"\n    {list[i]}");
					} else {
						sb.Append($",\n    {list[i]}");
					}
				} else {
					if (i == 0) {
						sb.Append(list[i].ToString());
					} else {
						sb.Append($", {list[i]}");
					}
				}
			}
			sb.Append(addLineBreaks ? "\n]" : " ]");
			return sb.ToString();
		}

		
		
#if NETSTANDARD
		/// <summary>
		/// Uses the new C# 8 Range syntax to access a range of values from any List&lt;T&gt;.<para />
		/// Unlike the built-in List.GetRange(), this version allows indices that are out of range
		///  and handles them gracefully.<para />
		/// 2023-04-18 - Jeremy Gibson Bond with help from ChatGPT
		/// </summary>
		/// <param name="list">Any list of elements</param>
		/// <param name="range">Use the new C# Range syntax</param>
		/// <returns>A new list with the specified elements</returns>
		static public List<T> GetRange<T>(this List<T> list, System.Range range) {
			// If the list is empty, just return an empty list
			if (list.Count == 0) return new List<T>();
			
			// Convert Range.Start and Range.End to indices
			int start = range.Start.Value;
			if (range.Start.IsFromEnd) 
				start = list.Count - range.Start.Value;
			int end = range.End.Value;
			if (range.End.IsFromEnd) 
				end = list.Count - range.End.Value;
		
			// Check to make sure that neither are out of range
			if (start < 0) start = 0;
			if (end >= list.Count) end = list.Count - 1;
			// If end is before start, swap them
			if (end < start) {
				(start, end) = (end, start); // I love this way of swapping values. :)
			}
		
			// Calculate the length of the range
			int length = end - start + 1;
		
			List<T> result = list.GetRange(start, length);
			return result;
		}
#endif
		/// <summary>
		/// Uses a Tuple instead of the new C# 8 Range syntax to access a range of values from any
		/// List&lt;T&gt;.<para />
		/// Unlike the built-in List.GetRange(), this version allows indices that are out of range
		///  and handles them gracefully.<para />
		/// 2023-04-18 - Jeremy Gibson Bond with help from ChatGPT
		/// </summary>
		/// <param name="list">Any list of elements</param>
		/// <param name="range">Use the new C# Range syntax</param>
		/// <returns>A new list with the specified elements</returns>
		static public List<T> GetRange<T>(this List<T> list, (int,int) range) {
			// If the list is empty, just return an empty list
			if (list.Count == 0) return new List<T>();
			
			// Convert Range.Start and Range.End to indices
			int start = range.Item1;
			if (range.Item1 < 0) 
				start = list.Count + range.Item1;
			int end = range.Item2;
			if (range.Item2 < 0) 
				end = list.Count - range.Item2;
		
			// Check to make sure that neither are out of range
			if (start < 0) start = 0;
			if (end >= list.Count) end = list.Count - 1;
			// If end is before start, swap them
			if (end < start) {
				(start, end) = (end, start); // I love this way of swapping values. :)
			}
		
			// Calculate the length of the range
			int length = end - start + 1;
		
			List<T> result = list.GetRange(start, length);
			return result;
		}

		#endregion
		
		
		#region ScriptableObject Extensions
		/// <summary>
		/// Creates and returns a clone of any given scriptable object.
		/// From: https://forum.unity.com/threads/create-copy-of-scriptableobject-during-runtime.355933/
		/// </summary>
		public static T Clone<T>(this T scriptableObject) where T : ScriptableObject
		{
			if (scriptableObject == null)
			{
				Debug.LogError($"Attempted to clone a ScriptableObject that was null. Returning default {typeof(T)} object.");
				return (T)ScriptableObject.CreateInstance(typeof(T));
			}
 
			T instance = UnityEngine.Object.Instantiate(scriptableObject);
			instance.name = scriptableObject.name; // remove (Clone) from name
			return instance;
		}
		
		#endregion
		
		
#region System.Type Extensions (Method Reflection - Probably will not compile into builds without [Preserve] attribute)

        /// <summary>
        /// Given a class/struct/interface Type thisType and a Type returnType, find the names of all public methods
        ///		of thisType that will return returnType.
        /// Also now included in XnUtils.cs - JGB 2023-04-11
        /// </summary>
        /// <param name="thisType">The Type on which to search for the public methods</param>
        /// <param name="returnType">The Type that the methods should return</param>
        /// <returns>A string List containing nullMethodName and all public methods on thisType that return returnType</returns>
        static public List<string> GetMethodsByReturnType( this Type thisType, Type returnType ) {
            return thisType.GetMethodsByReturnTypeAndParameters( thisType, returnType );
        }
        
        /// <summary>
        /// Given a class/struct/interface Type thisType and a Type returnType, find the names of all public methods
        ///		of thisType that will return returnType and accept parameterTypes as parameters.
        /// TODO: Also include in XnUtils.cs - JGB 2023-04-14
        /// </summary>
        /// <param name="thisType">The Type on which to search for the public methods</param>
        /// <param name="returnType">The Type that the methods should return</param>
        /// <param name="parameterTypes">The parameter Types that should be passed into the public method</param>
        /// <returns>A string List containing nullMethodName and all public methods on thisType that return returnType</returns>
        static public List<string> GetMethodsByReturnTypeAndParameters(this Type thisType, Type returnType, params Type[] parameterTypes) {
            MethodInfo[] methodInfos = thisType.GetMethods(BindingFlags.Public | BindingFlags.Instance);// | BindingFlags.DeclaredOnly);
            List<string> methodNames = new List<string>();
            bool paramsOk;
            for ( int i = 0; i < methodInfos.Length; i++ ) {
                if ( methodInfos[i].ReturnType == returnType ) {
                    paramsOk = true;
                    System.Reflection.ParameterInfo[] paramInfos = methodInfos[i].GetParameters();
                    // If it's not enough parameters, continue to the next method
                    if ( paramInfos.Length != parameterTypes.Length ) continue;
                    for ( int j = 0; j < parameterTypes.Length; j++ ) {
                        if ( paramInfos[j].ParameterType != parameterTypes[j] ) {
                            paramsOk = false;
                            break;
                        }
                    }
                    if (paramsOk) methodNames.Add( methodInfos[i].Name );
                }
                // methodInfos[i].CustomAttributes could be another way to find methods
            }
            return methodNames;
        }
        
        /// <summary>
        /// Given an object thisObject and a Type returnType, find the names of all public methods
        ///		of that object that will return returnType.
        ///  This was initially developed for XnAI, but seemed very useful outside of that. JGB 2023-04-11
        /// </summary>
        /// <param name="thisObject">The object on which to search for the public methods</param>
        /// <param name="returnType">The Type that the methods should return</param>
        /// <param name="parameterTypes"><b>OPTIONAL</b> The parameter Types that should be passed
        ///    into the public method</param>
        /// <returns>A string List of public methods on thisType that return returnType</returns>
        static public List<string> GetMethodsByReturnTypeAndParameters(this object thisObject, System.Type returnType, params Type[] parameterTypes) {
            return GetMethodsByReturnTypeAndParameters( thisObject.GetType(), returnType, parameterTypes );
        }

		/// <summary>
		/// Returns the most specific Type name (instead of the whole path to the System.Type).
		/// Example: System.Type would return "Type"
		/// </summary>
		/// <param name="type">A System.Type</param>
		/// <returns>The most specific type name without namespaces</returns>
        static public string ToStringShort( this Type type ) {
	        string[] bits = type.ToString().Split( '.' );
	        if ( bits.Length == 0 ) 
		        return type.ToString();
#if NETSTANDARD
	        return bits[^1]; // ^1 selects the last element in the Array if using .Net Standard 2.1 (C#8) or higher
#else
			return bits[bits.Length - 1];
#endif
		}
        
#endregion
		
	}
}
