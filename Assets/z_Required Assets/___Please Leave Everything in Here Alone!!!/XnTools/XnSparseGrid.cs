using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace XnTools {
    /// <summary>
    /// <para>A false grid that uses a Dictionary to store sparse data. If the grid is relatively full, then
    /// a multidimensional array would probably be better.</para>
    /// <para>NOTE: This could be reimplemented with a ulong for the Dictionary Key, which would allow:</para>
    /// <para>- 2D positive x,y values up to 2^32-1 (4,294,967,295)</para>
    /// <para>- 3D positive x,y,z values up to 2^21-1 (2,097,151)</para>
    /// <para>- 4D positive x,y,z, w values up to 2^16-1 (65,535)</para>
    /// </summary>
    /// <typeparam name="T">Any class or struct type</typeparam>
    public class XnSparseGrid<T> {
        private Dictionary<Vector3Int, T> _dict;
        private bool                      _nullable;
        private Vector3Int                _key; // Reusable Vector3Int so it doesn't have to be declared everywhere
        private T                         _defaultVal;
        
        public XnSparseGrid() {
            _dict = new Dictionary<Vector3Int, T>();
            _nullable = ( default(T) == null );
            _defaultVal = default(T);
        }
        
        public T this[ Vector3Int key ] {
            get {
                if ( _dict.ContainsKey( key ) ) {
                    return _dict[key];
                }
                // There was nothing in the _dict, so return the default value
                return default(T);
            }
            set {
                if ( value.Equals(_defaultVal) ) {
                    // If the value passed in is the default value (e.g., 0 or null), just remove that entry from _dict
                    _dict.Remove( key );
                } else {
                    _dict[key] = value;
                }
            }
        }
        
        public T this[ int x, int y, int z ] {
            get {
                _key = new Vector3Int( x, y, z );
                return this[_key];
            }
            set {
                _key = new Vector3Int( x, y, z );
                this[_key] = value;
            }
        }
        public T this[ int x, int y ] {
            get { return this[x, y, 0]; }
            set { this[x, y, 0] = value; }
        }
        public T this[ int x ] {
            get { return this[x, 0, 0]; }
            set { this[x, 0, 0] = value; }
        }

        public T this[ Vector2Int v2I ] {
            get { return this[v2I.x, v2I.y, 0]; }
            set { this[v2I.x, v2I.y, 0] = value; }
        }
       

        public int Count {
            get { return _dict.Count; }
        }

        public void Clear() {
            _dict.Clear();
        }

        public Vector3Int[] GetKeys3() {
            return _dict.Keys.ToArray();
        }

        public Vector2Int[] GetKeys2() {
            Vector2Int[] result = new Vector2Int[_dict.Keys.Count];
            int ndx = 0;
            foreach ( Vector3Int key in _dict.Keys ) {
                result[ndx++] = new Vector2Int( key.x, key.y );
            }
            return result;
        }

        public int[] GetKeys1() {
            int[] result = new int[_dict.Keys.Count];
            int ndx = 0;
            foreach ( Vector3Int key in _dict.Keys ) {
                result[ndx++] = key.x;
            }
            return result;
        }
        
        // NOTE: I couldn't figure out how to make a generic that accepted int, Vector2Int, or Vector3Int
        // public T1[] GetKeys<T1>() where T1 : struct {
        //     T1[] result = new T1[_dict.Keys.Count];
        //     int ndx = 0;
        //     foreach ( Vector3Int key in _dict.Keys ) {
        //         if ( typeof(T1) == typeof(Vector3Int) ) {
        //             T1[ndx] = key as T1;
        //         }
        //         Vector3Int[] v3Keys = _dict.Keys.ToArray();
        //         if ( typeof(T1) == typeof(int) ) { }
        //     }
        // }
        
        // NOTE: Remove() and RemoveAt() could be added here, but I don't believe that they are necessary - JGB 2023-04-04
    }
    
}