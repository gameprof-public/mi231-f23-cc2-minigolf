using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace XnTools {
    public class XnDepthCameraAdder : MonoBehaviour {


        /// <summary>
        /// Adds an "To Depth Camera" item to the context menu for InfoComponent
        /// </summary>
        /// <param name="command"></param>
        [MenuItem( "CONTEXT/Camera/Add XnDepthCamera Component", false, System.Int32.MaxValue )]
        static void AddXnDepthCameraComponent( MenuCommand command ) {
            Camera cam = (Camera) command.context;
            cam.gameObject.AddComponent<XnDepthCamera>();
        }
    }
}