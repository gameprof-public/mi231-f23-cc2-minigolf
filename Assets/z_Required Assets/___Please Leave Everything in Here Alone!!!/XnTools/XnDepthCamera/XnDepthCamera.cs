using System;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.Serialization;

// Source: This is based on the tutorial by FrozenMist Adventure at https://youtu.be/5lsKlOoepw4?si=fznX3iabXP9Xqdz9
// NOTE: This requires the XnScreenDepthNormals Shader 
namespace XnTools {


    [ExecuteInEditMode]
    [RequireComponent( typeof(Camera) )]
    public class XnDepthCamera : MonoBehaviour {
        private const string shaderDepth         = "Hidden/XnScreenDepth";
        private const string shaderNormalsWorld  = "Hidden/XnScreenDepthNormals"; 
        
        static public readonly int SP_DepthExponent          = Shader.PropertyToID("_DepthExponent");
        static public readonly int SP_CamToWorldNormalMatrix = Shader.PropertyToID("_CamToWorldNormalMatrix");

        
        [FormerlySerializedAs( "Cam" )]
        [HideInInspector]
        public Camera cam;

        public enum eXnDepthCameraMode { Scene, Depth, Normals };
        [OnValueChanged( "CameraModeChangedCallback" )]
        public eXnDepthCameraMode cameraMode = eXnDepthCameraMode.Scene;

        [Range(1,100)]
        public int depthMagnifier = 1;

        [ReadOnly]
        public Material generatedMaterial;
        

        void CameraModeChangedCallback() {
            if ( !IsCameraRefValid() ) return;
            
            generatedMaterial = null;

            switch ( cameraMode ) {
            case eXnDepthCameraMode.Scene:
                cam.depthTextureMode = DepthTextureMode.None;
                break;
            
            case eXnDepthCameraMode.Depth:
                cam.depthTextureMode = DepthTextureMode.DepthNormals;
                generatedMaterial = new Material( Shader.Find( shaderDepth ) );
                break;
            
            case eXnDepthCameraMode.Normals:
                cam.depthTextureMode = DepthTextureMode.DepthNormals;
                generatedMaterial = new Material( Shader.Find( shaderNormalsWorld ) );
                break;

            }
            
            // generatedMaterial.SetInt();
            
        }

        bool IsCameraRefValid() {
            if (cam == null) cam = this.GetComponent<Camera>();
            if ( cam == null ) {
                Debug.LogError( $"No Camera was found on the GameObject {gameObject.name}!" );
                return false;
            }
            return true;
        }
        
        
        
        // [Button]
        // void ConvertToDepthCamera() {
        //     Camera cam = GetComponent<Camera>();
        //     cam.depthTextureMode = DepthTextureMode.Depth;
        // }
        //
        // [Button]
        // void ConvertToRegularCamera() {
        //     Camera cam = GetComponent<Camera>();
        //     cam.depthTextureMode = DepthTextureMode.Depth;
        // }

        private void Update() {
            if ( cameraMode == eXnDepthCameraMode.Scene ) return;
            
            if (generatedMaterial == null) {
                CameraModeChangedCallback();
                if ( generatedMaterial == null ) {
                    Debug.LogError( $"The generatedMaterial was not able to be created by XnDepthCamera on {gameObject.name}!" +
                                    $" Resetting cameraMode to Scene." );
                    cameraMode = eXnDepthCameraMode.Scene;
                    return;
                }
            }
        }

        private void OnPreRender() {
            // If in the World Normals mode, send inverse camera transform to a global shader property
            if ( cameraMode == eXnDepthCameraMode.Normals ) {
                Shader.SetGlobalMatrix( SP_CamToWorldNormalMatrix, cam.cameraToWorldMatrix );
            }
        }

        private void OnRenderImage( RenderTexture source, RenderTexture destination ) {
            if ( generatedMaterial != null ) {
                generatedMaterial.SetFloat(SP_DepthExponent, depthMagnifier);
                Graphics.Blit( source, destination, generatedMaterial );
            } else {
                Graphics.Blit( source, destination );
            }
        }
    }
}

/*

using System. Collections;
using System. Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
[RequireComponent (typeof (Camera) )]
public class FMScreenDepthNormal: MonoBehaviour
{
    public Camera   Cam;
    public Material Mat;
    // Start is called before the first frame update
    void Start ()
    {
        / Update is called once per frame void Update ()
        if (Cam == null)
        {
            Cam = this. GetComponent<Camera> () ;
            Cam. depthTextureMode = DepthTextureMode. DepthNormals;
        }
        {
            if (Mat == null)
                //assign shader "Hidden/FMShader_ScreenDepthNormal" to Mat;
                Mat = new Material(Shader .Find("Hidden/FMShader_ScreenDepthNormal"));
        }
    }
    {
        private void OnRenderImage (RenderTexture source, RenderTexture destination)
        //render source to screen
        //Graphics.Blit (source, destination);
        //render source to screen with shader!
        Graphics. Blit (source, destination, Mat);
    }
    
*/