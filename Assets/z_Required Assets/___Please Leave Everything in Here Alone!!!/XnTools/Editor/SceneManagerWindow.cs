// From: https://docs.unity3d.com/ScriptReference/EditorBuildSettings-scenes.html

using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class SceneManagerWindow : EditorWindow {
    List<SceneAsset> m_SceneAssets = new List<SceneAsset>();

    // Add menu item named "Example Window" to the Window menu
    [MenuItem( "3PLE/Scene Manager Window" )]
    public static void ShowWindow() {
        //Show existing window instance. If one doesn't exist, make one.
        SceneManagerWindow smw = EditorWindow.GetWindow( typeof( SceneManagerWindow ) ) as SceneManagerWindow;
        smw.m_SceneAssets = new List<SceneAsset>();
        foreach (EditorBuildSettingsScene ebss in EditorBuildSettings.scenes) {
            if ( ebss.enabled ) {
                smw.m_SceneAssets.Add( AssetDatabase.LoadAssetAtPath<SceneAsset>( ebss.path ) );
            }
        }
    }

    void OnGUI() {
        GUILayout.Label( "Scenes to include in build:", EditorStyles.boldLabel );
        for ( int i = 0; i < m_SceneAssets.Count; ++i ) {
            m_SceneAssets[i] = (SceneAsset) EditorGUILayout.ObjectField( m_SceneAssets[i], typeof( SceneAsset ), false );
        }
        if ( GUILayout.Button( "Add" ) ) {
            m_SceneAssets.Add( null );
        }

        GUILayout.Space( 8 );

        if ( GUILayout.Button( "Apply To Build Settings" ) ) {
            SetEditorBuildSettingsScenes();
        }
    }

    public void SetEditorBuildSettingsScenes() {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        List<string> sceneNames = new List<string>();
        // Find valid Scene paths and make a list of EditorBuildSettingsScene
        List<EditorBuildSettingsScene> editorBuildSettingsScenes = new List<EditorBuildSettingsScene>();
        foreach ( var sceneAsset in m_SceneAssets ) {
            string scenePath = AssetDatabase.GetAssetPath( sceneAsset );
            if ( !string.IsNullOrEmpty( scenePath ) ) {
                editorBuildSettingsScenes.Add( new EditorBuildSettingsScene( scenePath, true ) );
                sceneNames.Add( sceneAsset.name );
            }
        }

        // Set the Build Settings window Scene list
        EditorBuildSettings.scenes = editorBuildSettingsScenes.ToArray();

        // Export scene list to Console
        sceneNames.Sort();
        sb.AppendLine( "________Scene Names________ (Copy everything after this line except _Main into Resources/SceneNames" );
        foreach (string s in sceneNames) {
            sb.AppendLine( s );
        }

        Debug.LogError( sb.ToString() );
    }
}