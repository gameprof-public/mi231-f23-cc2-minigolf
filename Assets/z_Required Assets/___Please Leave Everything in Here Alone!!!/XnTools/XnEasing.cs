// using UnityEngine;
// using System.Collections.Generic;

// I believe this class is now deprecated in favor of the XnInterpolation class. - JGB 2023-04-14
// The code is still here in case there's something in here that hasn't moved over yet. - JGB 2023-04-14
/*
namespace XnTools {
#region XnEasing Class
	//============================ XnEasing Classes ============================
	[System.Serializable]
	public class EasingCachedCurve {
		public List<string> curves =	new List<string>();
		public List<float>  mods   = 		new List<float>();
	}

	/// <summary>
	/// Class that can be used to create complex easing curves for linear interpolation functions.
	/// Terms include: Linear, In, Out, InOut, Sin, SinIn, & SinOut. All can be followed by a | and 
	/// a modifier number, and multiple easing operations can be comma separated.
	/// </summary>
	public class XnEasing {
		//[Tooltip("From XnEasing Terms include: Linear, In, Out, InOut, Sin, SinIn, & SinOut. " +
		//"All can be followed by a | and a modifier number, " +
		//"and multiple easing operations can be comma separated.")]
		static public string Linear = 		",Linear|";
		static public string In = 			",In|";
		static public string Out =			",Out|";
		static public string InOut = 		",InOut|";
		static public string Sin =			",Sin|";
		static public string SinIn =		",SinIn|";
		static public string SinOut =		",SinOut|";
		
		static public Dictionary<string,EasingCachedCurve> cache;
		// This is a cache for the information contained in the complex strings
		//   that can be passed into the Ease function. The parsing of these
		//   strings is most of the effort of the Ease function, so each time one
		//   is parsed, the result is stored in the cache to be recalled much 
		//   faster than a parse would take.
		// Need to be careful of memory leaks, which could be a problem if several
		//   million unique easing parameters are called
		
		static public float Ease( float u, params string[] curveParams ) {
			// Set up the cache for curves
			if (cache == null) {
				cache = new Dictionary<string, EasingCachedCurve>();
			}
			
			float u2 = u;
			foreach ( string curve in curveParams ) {
				// Check to see if this curve is already cached
				if (!cache.ContainsKey(curve)) {
					// If not, parse and cache it
					EaseParse(curve);
				} 
				// Call the cached curve
				u2 = EaseP( u2, cache[curve] );
			}
			return( u2 );
			/*	
				
				// It's possible to pass in several comma-separated curves
				string[] curvesA = curves.Split(',');
				foreach (string curve in curvesA) {
					if (curve == "") continue;
					//string[] curveA = 
				}
				
			}
			//string[] curve = func.Split(',');
			
			foreach (string curve in curves) {
				
			}
			
			string[] funcSplit;
			foreach (string f in funcs) {
				funcSplit = f.Split('|');
				
			}
			*
		}
		
		static private void EaseParse( string curveIn ) {
			EasingCachedCurve ecc = new EasingCachedCurve();
			// It's possible to pass in several comma-separated curves
			string[] curves = curveIn.Split(',');
			foreach (string curve in curves) {
				if (curve == "") continue;
				// Split each curve on | to find curve and mod
				string[] curveA = curve.Split('|');
				ecc.curves.Add(curveA[0]);
				if (curveA.Length == 1 || curveA[1] == "") {
					ecc.mods.Add(float.NaN);
				} else {
					float parseRes;
					if ( float.TryParse(curveA[1], out parseRes) ) {
						ecc.mods.Add( parseRes );
					} else {
						ecc.mods.Add( float.NaN );
					}
				}	
			}
			cache.Add(curveIn, ecc);
		}
		
		
		static public float Ease( float u, string curve, float mod ) {
			return( EaseP( u, curve, mod ) );
		}
		
		static private float EaseP( float u, EasingCachedCurve ec ) {
			float u2 = u;
			for (int i=0; i<ec.curves.Count; i++) {
				u2 = EaseP( u2, ec.curves[i], ec.mods[i] );
			}
			return( u2 );
		}


		
		
		static private float EaseP( float u, string curve, float mod ) {
			float u2 = u;
			
			switch (curve) {
			case "In":
				if (float.IsNaN(mod)) mod = 2;
				u2 = XnUtils.Pow(u, mod);
				break;
				
			case "Out":
				if (float.IsNaN(mod)) mod = 2;
				u2 = 1 - XnUtils.Pow( 1 -u, mod );
				break;
				
			case "InOut":
				if (float.IsNaN(mod)) mod = 2;
				if ( u <= 0.5f ) {
					u2 = 0.5f * XnUtils.Pow( u *2, mod );
				} else {
					u2 = 0.5f + 0.5f * (  1 - XnUtils.Pow( 1 -(2 *(u -0.5f)), mod )  );
				}
				break;
				
			case "Sin":
				if (float.IsNaN(mod)) mod = 0.15f;
				u2 = u + mod * Mathf.Sin( 2*Mathf.PI*u );
				break;
				
			case "SinIn":
				// mod is ignored for SinIn
				if ( float.IsNaN( mod ) ) mod = 0f;
				u2 = 1 - Mathf.Cos( u * Mathf.PI * 0.5f );
				u2 += Mathf.Sin( u * Mathf.PI ) * mod;
				break;
				
			case "SinOut":
				// mod is ignored for SinOut
				if ( float.IsNaN( mod ) ) mod = 0f;
				u2 = Mathf.Sin( u * Mathf.PI * 0.5f );
				u2 += Mathf.Sin( u * Mathf.PI ) * mod;
				break;
				
			case "Linear":
			default:
				// u2 already equals u
				break;
			}
			
			return( u2 );
		}
		
	}
	#endregion
}
*/