using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformMover : MonoBehaviour
{
    [SerializeField]
    public List<Transform> controlPoints;

    float t = 0.0f;
    public float speed = 2;
    public bool movingForward = true;

    private List<Vector3> pointPos;

    // Start is called before the first frame update
    void Start()
    {
        pointPos = new List<Vector3>();

        foreach (Transform controlPoint in controlPoints)
        {
            pointPos.Add(controlPoint.position);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        t += (movingForward ? 1 : -1) * Time.deltaTime * speed;

        t = Mathf.Clamp01(t);

        if ( t <= 0 || t >= 1)
        {
            movingForward = !movingForward;
        }

        Vector3 posOnCurve = BezierCurve(pointPos.ToArray(), t);

        transform.position = posOnCurve;
    }

    Vector3 BezierCurve(Vector3[] pts, float t)
    {

        int numPts = pts.Length;

        Vector3[] temp = new Vector3[numPts];

        for (int i = 0; i < numPts; i++)
        {
            temp[i] = pts[i];
        }

        for(int j = 1; j < numPts; j++)
        {
            for (int i = 0; i < numPts - j; i++)
            {
                temp[i] = (1 - t) * temp[i] + t * temp[i + 1];
            }
        }

        return temp[0];
    }
}
