using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CupSunkSensor : MonoBehaviour {
    private Hole hole;
    
    void Start() {
        // Find if there is a Hole parent
        // GetComponentInParent<>() is a relatively slow operation, but if you only do it once, it's not a big deal. 
        hole = GetComponentInParent<Hole>();
    }
    
    private void OnTriggerEnter( Collider other ) {
        Golfball ball = other.GetComponent<Golfball>();
        if ( ball != null ) {
            hole?.BallSunk( this, ball );
        }
    }
}
