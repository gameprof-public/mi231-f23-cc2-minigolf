using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Golfball : MonoBehaviour {
    static private Golfball _S;

    [Header("Inscribed")]
    public float maxPuttVelocity   = 10;
    public float minVelBeforeSleep = 0.1f;
    public int   framesBeforeSleep = 4;

    private Rigidbody rb;
    private float originalSpeed;

    [Header( "Dynamic" )]
    public int currentHoleNum = 0;
    

    private int currFramesBeforeSleep;
    
    private Rigidbody rigid;

    private Vector3 lastPuttPosition;
    
    // Start is called before the first frame update
    void Start() {
        _S = this;
        rigid = GetComponent<Rigidbody>();
        lastPuttPosition = transform.position;
        currFramesBeforeSleep = framesBeforeSleep;
        currentHoleNum = 0;
        JumpToNextHole();

        rb = GetComponent<Rigidbody>();
        originalSpeed = rb.velocity.magnitude;
    }

    public void JumpToNextHole() {
        currentHoleNum = Hole.GetNextHole( currentHoleNum );
        Hole hole = Hole.HOLES[currentHoleNum];
        transform.position = hole.dropPoint;
        rigid.WakeUp();
    }

    void Update() {
        if ( Input.GetKeyDown( KeyCode.R ) ) {
            // Reset the ball position to last putt position
            ResetToLastPuttPosition();
        }
    }

    private void FixedUpdate() {
        // If the ball slows enough to sleep
        if ( !rigid.IsSleeping() && ( rigid.velocity.magnitude <= minVelBeforeSleep ) ) {
            currFramesBeforeSleep--;
            if ( currFramesBeforeSleep <= 0 ) {
                rigid.velocity = Vector3.zero;
                rigid.Sleep();
            }
            Putter.MOVE_TO( transform.position );
        } else {
            currFramesBeforeSleep = framesBeforeSleep;
        }
    }

    void ResetToLastPuttPosition() {
        rigid.velocity = Vector3.zero;
        transform.position = lastPuttPosition;
    }

    // Update is called once per frame
    void Putt(Vector3 puttVel) {
        lastPuttPosition = transform.position;
        rigid.velocity = puttVel * maxPuttVelocity;
    }

    static public void PUTT( Vector3 puttVel ) {
        _S?.Putt( puttVel );
    }

    public void ApplySpeedBoost(float boostAmount, float boostDuration)
    {
        // Calculate the new speed after the boost.
        float newSpeed = originalSpeed * boostAmount;

        // Apply the new speed to the golf ball.
        rb.velocity = rb.velocity.normalized * newSpeed;

        // Optionally, set a timer to revert to the original speed after a duration.
        StartCoroutine(RevertSpeedAfterDelay(boostDuration));
    }

    private IEnumerator RevertSpeedAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);

        // Revert to the original speed.
        rb.velocity = rb.velocity.normalized * originalSpeed;
    }
}
