using System;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;

/// <summary>
/// This script handles the sensor that will move the Golfball from the Golfball layer
/// to the GolfballNearCup layer and from it if the Golfball gets too far away.
/// </summary>
[RequireComponent( typeof( CapsuleCollider ) )]
public class Cup : MonoBehaviour {
    [Layer]
    public int layerWhenFar, layerWhenNear;

    private CapsuleCollider thisColl;

    void Start() {
        thisColl = GetComponent<CapsuleCollider>();
    }
    
    void OnTriggerEnter(Collider coll) {
        OnTriggerStay(coll);
    }

    void OnTriggerStay( Collider coll ) {
        // Get a reference to the coll's GameObject
        GameObject go = coll.gameObject;
        
        // If the colliding GameObject go is already in the layerWhenNear layer, don't need to check any more
        if ( go.layer == layerWhenNear ) {
            // Just return from this method
            return;
        }
        
        // Check to see whether the position of the other object is within this collider
        Vector3 localPos = transform.InverseTransformPoint( go.transform.position );
        // Flatten localPos.y to 0 to just test the distance from the center of the cup the XZ plane  
        localPos.y = 0; 
        // Check to see if the localPos magnitude is <= the radius of the thisColl CapsuleCollider
        if ( localPos.magnitude <= thisColl.radius ) {
            // Move the Golfball to the layerWhenNear
            // This will cause it to ONLY collide with the GolfballNearCup layer (falling through the Green).
            go.layer = layerWhenNear;
        }
    }

    private void OnTriggerExit( Collider coll ) {
        // Get a reference to the coll's GameObject
        GameObject go = coll.gameObject;
        
        if ( go.layer == layerWhenNear ) {
            // Move the Golfball to the layerWhenFar
            // This will cause it collide with things other than the GolfballNearCup layer
            go.layer = layerWhenFar;
        }
    }
}
