using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spin : MonoBehaviour
{
    [SerializeField]
    float rotationSpeed = 0;
    [SerializeField]
    Vector3 rotationDir = new Vector3();

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(rotationSpeed * rotationDir * Time.deltaTime);
    }
}
