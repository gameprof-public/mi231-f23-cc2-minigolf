using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OriginalPlatformMover : MonoBehaviour
{ 
   [SerializeField]
    public Vector3[] points;

    private Vector3 platformPos;
    private Vector3 p1;
    private Vector3 p2;

    float t = 0.0f;
    public float speed = 2;
    public bool movingForward = true;

// Start is called before the first frame update
void Start()
    {
        GameObject platformGO = GameObject.Find("Platform");
        GameObject point1 = GameObject.Find("point1");
        GameObject point2 = GameObject.Find("point2");

        platformPos = platformGO.transform.position;
        p1 = point1.transform.position;
        p2 = point2.transform.position;

        points = new Vector3[3];
        points[0] = platformPos;
        points[1] = p1;
        points[2] = p2;

    }

    // Update is called once per frame
    void Update()
    {
        t += (movingForward ? 1 : -1) * Time.deltaTime * speed;

        t = Mathf.Clamp01(t);

        if (t <= 0 || t >= 1)
        {
            movingForward = !movingForward;
        }

        Vector3 posOnCurve = BezierCurve(points[0], points[1], points[2], t);

        transform.position = posOnCurve;
    }

    Vector3 BezierCurve(Vector3 p0, Vector3 p1, Vector3 p2, float t)
    {
        float u = 1 - t;
        float tt = t * t;
        float uu = u * u;
        float uuu = uu * u;
        float ttt = tt * t;

        Vector3 p = uuu * p0;
        p += 3 * uu * t * p1;
        p += 3 * u * tt * p2;
        p += ttt * p2;

        return p;
    }
}
