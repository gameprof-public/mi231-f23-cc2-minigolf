using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using Cinemachine;
using NaughtyAttributes;
using UnityEngine.Serialization;
using XnTools;
using Heading = Cinemachine.CinemachineOrbitalTransposer.Heading;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

public class OrbitalCameraControl : MonoBehaviour {
    [SerializeField][NaughtyAttributes.ReadOnly]
    private CinemachineOrbitalTransposer orbital;

    [Header("Inscribed")]
    [OnValueChanged( "UpdateOffsetsList" )]
    public List<Vector3> offsetsList;
    
    [OnValueChanged( "UpdateOffsetU" )]
    [Range(0f,1f)]
    public float           initialOffsetU;
    
    [FormerlySerializedAs( "splineWeighting" )]
    [OnValueChanged( "UpdateOffsetSplineWeighting" )]
    [Tooltip("Default is 0.5=Centripetal Spline. 0=Uniform Spline & 1=Chordic Spline")]
    [Range(0f,1f)]
    public float splineAlpha = 0.5f;
    
    [Tooltip("Smooths between a linear interpolation (0) and full spline (1).")]
    [Range( 0f, 1f )]
    public float splineWeight = 0.5f;
    
    [Range(-180,180)]
    [OnValueChanged( "UpdateHeadingBias" )]
    public float initialHeadingBias       = 0;
    [FormerlySerializedAs( "mousePixelPerRotDeg" )]
    [FormerlySerializedAs( "mouseRotDegPerPixel" )]
    public float mousePixelsPerRotDeg = 10;
    public float   mousePixelsPerU   = 400;
    public Vector2 mouseLookDeadZone = new Vector2( 50, 100 );
    
    [Header("Dynamic")]
    [NaughtyAttributes.ReadOnly] [SerializeField]
    private float _currHeadingBias = 0;
    [NaughtyAttributes.ReadOnly] [SerializeField]
    private float _offsetU = 0;
    [NaughtyAttributes.ReadOnly] [SerializeField]
    private Vector3 _currOffset;

    public float currHeadingBias {
        get { return _currHeadingBias; }
        set {
            _currHeadingBias = value;
            if ( !FindOrbitalTransposer() ) return;
            Heading h = new Heading( Heading.HeadingDefinition.WorldForward, 0, _currHeadingBias );
            orbital.m_Heading = h;
        }
    }

    public float offsetU {
        get { return _offsetU; }
        set {
            _offsetU = value;
            currOffset = InterpolateOffset(_offsetU);
        }
    }
    public Vector3 currOffset {
        get { return _currOffset; }
        set {
            _currOffset = value;
            if ( !FindOrbitalTransposer() ) return;
            orbital.m_FollowOffset = _currOffset;
        }
    }

    private XnUtils.CentripetalCatmullRomSpline spline = null;
    
    // void UpdateOffsetList() {
    //         
    // }
    //
    // void UpdateOffsetSteps() {
    //     if ( !FindOrbitalTransposer() ) return;
    //     orbital.m_FollowOffset = baseFollowOffset;
    //     currHeadingBias = toHeadingBias;
    // }

    void UpdateOffsetsList() {
        if ( !ValidateSettings() ) return;
    }
    
    void UpdateOffsetU() {
        if ( !FindOrbitalTransposer() ) return;
        if ( !ValidateSettings() ) return;
        currOffset = InterpolateOffset();
    }

    void UpdateOffsetSplineWeighting() {
        if ( !FindOrbitalTransposer() ) return;
        if ( !ValidateSettings() ) return;
        spline.alpha = splineAlpha;
        currOffset = InterpolateOffset();
    }

    void UpdateHeadingBias() {
        currHeadingBias = initialHeadingBias;
    }

    [Button]
    void UpdateSplinePoints() {
        spline = null;
        UpdateOffsetSplineWeighting();
    }

    bool ValidateSettings() {
        if ( offsetsList == null || offsetsList.Count == 0 ) {
            return false;
        }
        if ( offsetsList.Count > 2  && spline == null) {
            spline = new XnUtils.CentripetalCatmullRomSpline( offsetsList.ToArray(), 0.5f, true );
        }
        return true;
    }


    // Vector3 CalculateTween( Vector3 vA, Vector3 vB, Vector3 vC ) {
    //     Vector3 vAB = vB - vA;
    //     float magAB = vAB.magnitude;
    //     Vector3 dirAB = vAB.normalized;
    //     Vector3 vAC = vC - vA;
    //     float dotAC = Vector3.Dot( dirAB, vAC );
    //     Vector3 perp = vC - (vA + (dirAB * dotAC));
    //     Vector3 midAB = vA + (dirAB * (magAB * 0.5f));
    //     Vector3 tween = midAB - (perp * splineWeighting);
    //     return tween;
    // }
    
    
    // Start is called before the first frame update
    void Start() {
        if ( !FindOrbitalTransposer() ) return;
        if ( ValidateSettings() ) { }
        currHeadingBias = initialHeadingBias;
        offsetU = initialOffsetU;
    }

    bool FindOrbitalTransposer() {
        CinemachineVirtualCamera vCam = GetComponentInChildren<CinemachineVirtualCamera>();
        if ( vCam == null ) {
            Debug.LogError($"{gameObject.name} or one of its children must have a Cinemachine Virtual Camera component.");
            return false;
        }
        orbital = vCam.GetCinemachineComponent<CinemachineOrbitalTransposer>();
        if ( orbital == null ) {
            Debug.LogError($"The CinemachineVirtualCamera on {vCam.gameObject.name} (a child of {gameObject.name})" +
                           $" must have an OrbitalTransposer Cinemachine component.");
            return false;
        }
        return true;
    }

    Vector3 InterpolateOffset( float u = float.NegativeInfinity ) {
        if ( float.IsNegativeInfinity(u) ) u = initialOffsetU;
        u = Mathf.Clamp01( u );
        // Need to perform non-Bezier interpolation to ensure that all points are hit.
        switch ( offsetsList.Count ) {
        case 0:
            // This code should never be executed because ValidateSettings will fail before it gets here.
            return default(Vector3);
            break;
        
        case 1:
            return offsetsList[0];
            break;
        
        case 2:
            return Vector3.Lerp( offsetsList[0], offsetsList[1], u );
            break;
        
        default:
            // Calculate uPart and uNdx
            int numSegments = offsetsList.Count - 1;
            float uExpanded = u * numSegments;
            int sNum = (int) uExpanded;
            float uPart = uExpanded % 1f;
            if ( sNum == numSegments ) {
                sNum = numSegments - 1;
                uPart = 1f;
            }
            Vector3 vLinear = Vector3.Lerp( offsetsList[sNum], offsetsList[sNum+1], uPart );
            Vector3 vSpline = spline.Interpolate( u );
            return Vector3.Lerp( vLinear, vSpline, splineWeight );
            break;
        }
    }


    private bool    isRightDragging = false;
    private Vector3 mousePos2DDown;
    private float   headingBiasDown, offsetUDown;
    void Update() {
        if ( !isRightDragging ) {
            if ( Input.GetMouseButtonDown( 1 ) ) {
                mousePos2DDown = Input.mousePosition;
                headingBiasDown = currHeadingBias;
                offsetUDown = offsetU;
                isRightDragging = true;
            }
        } else {
            if ( Input.GetMouseButtonUp( 1 ) ) {
                isRightDragging = false;
            } else {
                // Actually drag the camera
                Vector3 deltaRaw = Input.mousePosition - mousePos2DDown;
                // Adjust delta to account for dead zone
                Vector3 delta = deltaRaw;
                float xSign = delta.x < 0 ? -1 : 1;
                float ySign = delta.y < 0 ? -1 : 1;
                if ( delta.x * xSign > mouseLookDeadZone.x ) {
                    delta.x -= xSign * mouseLookDeadZone.x;
                } else {
                    delta.x = 0;
                }
                if ( delta.y * ySign > mouseLookDeadZone.y ) {
                    delta.y -= ySign * mouseLookDeadZone.y;
                } else {
                    delta.y = 0;
                }
                
                // Adjust heading bias based on X axis
                currHeadingBias = headingBiasDown + delta.x / mousePixelsPerRotDeg;
                // Adjust offset u based on Y axis
                offsetU = offsetUDown + delta.y / mousePixelsPerU;
            }
        }
    }
    
    
}
