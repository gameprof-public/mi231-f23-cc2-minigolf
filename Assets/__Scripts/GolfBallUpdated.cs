using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GolfBallUpdated : MonoBehaviour
{ 

    static private GolfBallUpdated _S;

    public float maxPuttVelocity = 10;
    public float minVelBeforeSleep = 0.1f;
    public int framesBeforeSleep = 4;
    public float threashhold = 0f;


    private int currFramesBeforeSleep;

    private Rigidbody rigid;

    private Vector3 lastPuttPosition;

    // Start is called before the first frame update
    void Start()
    {
        _S = this;
        rigid = GetComponent<Rigidbody>();
        lastPuttPosition = transform.position;
        currFramesBeforeSleep = framesBeforeSleep;
        rigid.WakeUp();
    }

    void Update()
    {
        if (transform.position.y < threashhold)
        {
            Debug.Log("Your Below the Map: " + transform.position.y);
            ResetToLastPuttPosition();
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            // Reset the ball position to last putt position
            ResetToLastPuttPosition();
        }
    }

    private void FixedUpdate()
    {
        // If the ball slows enough to sleep
        if (!rigid.IsSleeping() && (rigid.velocity.magnitude <= minVelBeforeSleep))
        {
            currFramesBeforeSleep--;
            if (currFramesBeforeSleep <= 0)
            {
                rigid.velocity = Vector3.zero;
                rigid.Sleep();
            }
            Putter.MOVE_TO(transform.position);
        }
        else
        {
            currFramesBeforeSleep = framesBeforeSleep;
        }
    }

    void ResetToLastPuttPosition()
    {
        rigid.velocity = Vector3.zero;
        transform.position = lastPuttPosition;
    }

    // Update is called once per frame
    void Putt(Vector3 puttVel)
    {
        lastPuttPosition = transform.position;
        rigid.velocity = puttVel * maxPuttVelocity;
    }

    static public void PUTT(Vector3 puttVel)
    {
        _S?.Putt(puttVel);
    }
}

