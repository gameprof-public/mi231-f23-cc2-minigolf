using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class DropPoint : MonoBehaviour {
    
#if UNITY_EDITOR
    private void OnDrawGizmos() {
        Gizmos.color = Color.white;
        Vector3 pos = transform.position;
        Vector3 pos2 = pos + Vector3.down * 20;
        Gizmos.DrawSphere( pos, 4 );
        // Gizmos.DrawRay( pos, Vector3.down );
        Handles.color = Color.white;
        Handles.DrawAAPolyLine(4, new [] { pos, pos2 });
        Handles.ArrowHandleCap(101, pos, Quaternion.Euler( 90, 0, 0 ), 20, EventType.Repaint );
    }
#endif
    
}
