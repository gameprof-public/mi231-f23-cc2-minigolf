using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawn : Golfball
{
    [Header("Inscribed")]
    public float threashhold = 0f;
    public Vector3 respawnPoint = new Vector3(0, 0, 0);
    // Update is called once per frame
    void Update()
    {
        if(transform.position.y < threashhold)
        {
            Debug.Log("Your Below the Map");
            transform.position = respawnPoint;
            
        }
    }
}
