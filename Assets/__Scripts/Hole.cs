using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using UnityEngine.UI;

public class Hole : MonoBehaviour {
    static public Hole[] HOLES;

    [Range(0,18)]
    public int holeNum = 0;
    public Vector3 dropPoint;

    private int holeProgress = 0;

    // Add a reference to the progress bar slider in the Unity Inspector.
    [SerializeField]
    private Slider progressBarSlider;

    void Awake() {
        // Register this Hole with the HOLES array
        if ( HOLES == null ) {
            HOLES = new Hole[18];
        }

        if ( holeNum == 0 ) {
            Debug.LogWarning($"The hole {gameObject.name} has a holeNum={holeNum}," +
                             $" so it will be ignored and not played.");
            return;
        }
        if ( HOLES[holeNum] != null ) {
            Debug.LogWarning($"Both the hole {HOLES[holeNum].gameObject.name} and {gameObject.name}" +
                             $" have the holeNum={holeNum}, so {gameObject.name} will be ignored and not played.");
            return;
        }

        HOLES[holeNum] = this;

        DropPoint dpGO = GetComponentInChildren<DropPoint>();
        if ( dpGO == null ) {
            dropPoint = transform.position + Vector3.up * 5;
        } else {
            dropPoint = dpGO.transform.position;
        }
    }

    public void BallSunk( CupSunkSensor cupSunkSensor, Golfball ball ) {
        Debug.Log( $"The ball {ball} sank in the cup {cupSunkSensor}." );
        UpdateProgressBar();
        ball.JumpToNextHole();
    }

    public Vector3 GetDropPoint() {
        return dropPoint;
    }

    static public int GetNextHole( int currentHoleNum ) {
        if ( HOLES == null ) {
            Debug.LogError( $"GetNextHole was called before HOLES was initialized." );
            return 0;
        }
        int loopBreaker = 100;
        do {
            currentHoleNum++;
            if ( currentHoleNum >= HOLES.Length ) {
                // TODO: This just loops the HOLES, but could add code to end game when all holes have been played
                currentHoleNum = 1;
            }
            loopBreaker--;
        } while ( HOLES[currentHoleNum] == null && loopBreaker > 0 );
        if ( loopBreaker <= 0 ) {
            // Something went wrong
            Debug.LogError( "The do...while loop in GetNextHole turned into an infinite loop!" );
            return 0;
        }
        return currentHoleNum;
    }

    private void UpdateProgressBar()
    {
        if (progressBarSlider != null)
        {
            // Normalize the progress to the slider's 0-1 range.
            float normalizedProgress = holeProgress / 100f; // Adjust this as needed.
            progressBarSlider.value = normalizedProgress;
        }
    }
}
