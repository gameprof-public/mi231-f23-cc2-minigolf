using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;

public class Putter : MonoBehaviour {
    // Add a private Singleton for PuttingScript because only one should ever be active.
    static private Putter _S;
    static private bool SHOW_DEBUG = false;
    
    [Header( "Inscribed" )]
    public SpriteRenderer puttingRingSRend;
    public LineRenderer   lineRend;
    public Transform      strengthPointer;
    public AnimationCurve strengthCurve;

    [FormerlySerializedAs( "puttVec" )]
    [Header( "Dynamic" )]
    public Vector3 puttVel;

    private SphereCollider puttingSphere;
    
    public enum ePuttingState { idle, inSphere, putting, waiting };
    [SerializeField]
    private ePuttingState _state = ePuttingState.idle;
    public ePuttingState state {
        get { return _state; }
        private set {
            _state = value;
            switch ( _state ) {
                case ePuttingState.idle:
                case ePuttingState.waiting:
                    visible = false;
                    break;
                
                case ePuttingState.inSphere:
                    puttingRingSRend.enabled = true;
                    break;
                
                case ePuttingState.putting:
                    visible = true;
                    break;
            }
        } 
    }

    [SerializeField]
    private bool _visible = false;
    public bool visible {
        get { return _visible; }
        set {
            _visible = value;
            puttingRingSRend.enabled = _visible;
            lineRend.enabled = _visible;
            strengthPointerSRend.enabled = _visible;
        }
    }
    
    
    // A LayerMask is actually an int, and Unity treats it as one, so we use the int type here instead of LayerMask
    private int            puttingRingLayerMask;
    private Transform      puttPointer;
    private SpriteRenderer strengthPointerSRend;


    void Start() {
        _S = this;
        puttingRingLayerMask = LayerMask.GetMask( "PuttingRing" );
        puttingSphere = GetComponent<SphereCollider>();
        lineRend.positionCount = 2; // There will always be only 2 positions for the LineRenderer
        lineRend.SetPosition( 0, Vector3.zero );
        strengthPointerSRend = strengthPointer.GetComponent<SpriteRenderer>();
        HidePuttStrength();
    }
    
    // Start is called before the first frame update
    void OnMouseEnter() {
        switch ( state ) {
        case ePuttingState.idle:
            state = ePuttingState.inSphere;
            break;
            
        default:
            // Do nothing
            break;
        }
    }

    void OnMouseExit() {
        if ( state == ePuttingState.inSphere ) {
            state = ePuttingState.idle;
        }
    }

    void Update() {
        switch ( state ) {
        case ePuttingState.inSphere:
            if ( Input.GetMouseButtonDown( 0 ) ) {
                state = ePuttingState.putting;
                DrawPuttStrength();
            }
            break;
        
        case ePuttingState.putting:
            if ( Input.GetMouseButtonUp( 0 ) ) {
                state = ePuttingState.waiting;
                HidePuttStrength();
                
                // Actually putt the golfball
                float strength = puttVel.magnitude;
                float adjustedStrength = strengthCurve.Evaluate( strength );
                Vector3 adjustedPuttVel = puttVel.normalized * adjustedStrength;
                Golfball.PUTT( adjustedPuttVel );
                
                break; // This breaks out of the case and exits the switch statement
            }
            // The code from here to the break; statement is skipped if the previous if clause was executed. 
            // Show the strength of the putt
            DrawPuttStrength();
            break;
        
        default:
            // Do nothing
            break;
        }
    }

    void DrawPuttStrength() {
        // Determine where the mouse pointer is on the putt ring
        Vector3 mousePosScreen = Input.mousePosition;
        Ray mouseRay = Camera.main.ScreenPointToRay( mousePosScreen );
        RaycastHit hit;
        if ( Physics.Raycast( mouseRay, out hit, Single.PositiveInfinity, puttingRingLayerMask ) ) {
            // We know that the mouse hit a collider in the puttingRingLayerMask layer, and there is only one of those.
            Vector3 hitPoint = hit.point;
            if (SHOW_DEBUG) Debug.DrawLine(transform.position, hitPoint);
            // Copy the world z position from the Putter to make sure that it is in the same plane as the putter 
            hitPoint.y = transform.position.y;
            if (SHOW_DEBUG) Debug.DrawLine(transform.position, hitPoint, Color.magenta);
            // Then set the world position of the strengthPointer
            strengthPointer.position = hitPoint;
            // Now, we want to make sure that the hitPoint is within the PuttingRing, which has a local radius of 1
            Vector3 localPos = strengthPointer.localPosition;
            if ( localPos.magnitude > 1 ) {
                localPos.Normalize();
                strengthPointer.localPosition = localPos;
            }
            // Now draw the LineRenderer
            lineRend.SetPosition( 1, localPos );
            
            // Make everything visible
            visible = true;
            
            // Set the puttVec
            puttVel = -strengthPointer.localPosition;
        } else {
            // The mouse is too far from the putting ring to register, so we should call HidePuttStrength()
            HidePuttStrength();
        }
    }

    void HidePuttStrength() {
        visible = false;
    }
    

    private void MoveTo( Vector3 pos ) {
        transform.position = pos;
        state = ePuttingState.idle;
        // Check to see if the mouse is already inside the putting sphere
        Vector3 mousePosScreen = Input.mousePosition;
        Ray mouseRay = Camera.main.ScreenPointToRay( mousePosScreen );
        RaycastHit hit;
        if ( Physics.Raycast( mouseRay, out hit, Single.PositiveInfinity ) ) {
            if ( hit.collider == puttingSphere ) {
                state = ePuttingState.inSphere;
            }
        }
    }

    static public void MOVE_TO( Vector3 pos ) {
        _S.MoveTo(pos);
    }
}
